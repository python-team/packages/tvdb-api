tvdb-api (3.1-3) unstable; urgency=medium

  * debian/control
    - run wrap-and-sort
    - add pytest, pytest-cov, pytest-env, requests, requests-cache to b-d,
      needed by tests
    - add, commented out, Testsuite: autopkgtest-pkg-pybuild, waiting for
      pytest-env
  * debian/patches/PR105.patch
    - add patch to make this module compatible with recent requests_cache
      releaess;  Closes: #1039968
  * debian/rules
    - only run tests during autopkgtest
  * debian/pybuild.testfiles
    - setup necessary tests files for autopkgtest

 -- Sandro Tosi <morph@debian.org>  Tue, 08 Aug 2023 01:32:01 -0400

tvdb-api (3.1-2) unstable; urgency=medium

  * debian/patches/PR97.patch
    - fix working with newer requests-cache versions; Closes: #1005393

 -- Sandro Tosi <morph@debian.org>  Sat, 26 Feb 2022 21:22:16 -0500

tvdb-api (3.1-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * Use the new Debian Python Team contact name and address
  * debian/copyright
    - extend packaging copyright years

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sun, 10 Oct 2021 23:16:16 -0400

tvdb-api (3.0.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * New upstream release
  * debian/watch
    - track github releases
  * debian/copyright
    - update to new upstream release
    - extend packaging copyright years
  * debian/rules
    - install README.md

 -- Sandro Tosi <morph@debian.org>  Sun, 25 Oct 2020 19:49:01 -0400

tvdb-api (2.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * drop Python 2 support + use pybuild + skip tests
  * debian/control
    - bump Standards-Version to 4.4.0 (no changes needed)
    - drop obsolete XS-Python-Version field
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sun, 18 Aug 2019 11:42:50 -0400

tvdb-api (2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Sandro Tosi ]
  * New upstream release
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.1.4 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sun, 10 Jun 2018 21:44:23 -0400

tvdb-api (1.10-1) unstable; urgency=medium

  * New upstream release
  * debian/watch
    - use PyPI redirector
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 3.9.6 (no changes needed)
    - set me as Maintainer, team as Uploaders
    - add requests/requests-cache to py3k package
  * switch from pysupport to dh_python2; Closes: #786269
  * bump dh compat to 9
  * debian/{control, rules}
    - build py3k package

 -- Sandro Tosi <morph@debian.org>  Fri, 05 Jun 2015 21:09:49 -0400

tvdb-api (1.9-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - bump Standards-Version to 3.9.5 (no changes needed)
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Jan 2014 18:34:29 +0100

tvdb-api (1.8.2-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Add Vcs-* fields.

  [ Sandro Tosi ]
  * debian/control
    - bump Standards-Version to 3.9.4 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sun, 29 Sep 2013 15:42:33 +0200

tvdb-api (1.8.2-1) experimental; urgency=low

  * New upstream release
  * debian/copyright
    - extended packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 05 Jan 2013 23:51:35 +0100

tvdb-api (1.7.2-1) experimental; urgency=low

  * New upstream release

 -- Sandro Tosi <morph@debian.org>  Sat, 11 Aug 2012 14:48:41 +0200

tvdb-api (1.7.1-1) unstable; urgency=low

  * New upstream release
  * debian/readme.md
    - removed, now included upstream
  * debian/copyright
    - updated

 -- Sandro Tosi <morph@debian.org>  Thu, 17 May 2012 22:02:00 +0200

tvdb-api (1.6.3-1) unstable; urgency=low

  * Initial release (Closes: #668083)

 -- Sandro Tosi <morph@debian.org>  Sun, 08 Apr 2012 20:33:54 +0200
